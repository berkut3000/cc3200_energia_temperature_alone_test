#define TARGET_IS_CC3101

// getTemp() function for cc3200
#ifdef TARGET_IS_CC3101
#include <Wire.h>
#include "Adafruit_TMP006.h"
Adafruit_TMP006 tmp006(0x41);
#endif

void setup() {

  Serial.begin(115200);
  // attempt to connect to Wifi network:

#ifdef TARGET_IS_CC3101
  if (!tmp006.begin()) {
    Serial.println("No sensor found");
    while (1);
  }
#endif
}

void loop() {


  float x = getTemp();

   
  int xint = x*100;
  int xd =  xint/1000;
  int xu = (xint%1000)/100;
  int xd1 = (xint%100)/10;
  int xd2 = (xint%10);
  
  Serial.println(xint);
  Serial.println(xd);
  Serial.println(xu);
  Serial.println(xd1);
  Serial.println(xd2);
  
}

//// getTemp() function for MSP430F5529
//#if defined(__MSP430F5529)
//// Temperature Sensor Calibration-30 C
//#define CALADC12_15V_30C  *((unsigned int *)0x1A1A)
//// Temperature Sensor Calibration-85 C
//#define CALADC12_15V_85C  *((unsigned int *)0x1A1C)
//
//double getTemp() {
//  return (float)(((long)analogRead(TEMPSENSOR) - CALADC12_15V_30C) * (85 - 30)) /
//    (CALADC12_15V_85C - CALADC12_15V_30C) + 30.0f;
//}
//
//// getTemp() function for Stellaris and TivaC LaunchPad
//#elif defined(TARGET_IS_SNOWFLAKE_RA0) || defined(TARGET_IS_BLIZZARD_RB1)
//
//double getTemp() {
//  return (float)(147.5 - ((75 * 3.3 * (long)analogRead(TEMPSENSOR)) / 4096));
//}

// getTemp() function for cc3200
#if defined(TARGET_IS_CC3101)
double getTemp() {
  return (double)tmp006.readObjTempC();
}
#else
double getTemp() {
  return 21.05;
}
#endif
